import express from "express";
const app = express();
const port = 4000;

app.get("/", (req, res) => {
  res.send("CI CD Journey Start");
});

app.listen(port, () => {
  console.log(`Application is running ${port}`);
});
